//
//  DrawViewController.m
//  VinoOne
//
//  Created by Ethan on 2014/10/26.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#import "DrawViewController.h"
#import <Accelerate/Accelerate.h>

@implementation DrawViewController

@synthesize effect = _effect;
@synthesize audioController = _audioController;

-(id)initWithAudioController:(AudioController*)controller{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    self = [storyboard instantiateViewControllerWithIdentifier:@"myGlkVC"];
    if(self){
        _audioController = controller;
        _effect = [[GLKBaseEffect alloc] init];
        GLKView * view = (GLKView *)self.view;
        view.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        for(int i = 0 ; i < 2; i ++){
            mIdx[i] = 0;
            mLen[i] = [_audioController getBufferManagerInstance]->DrawBufferLen(i);
            mBuf[i] = new GLfloat[2 * mLen[i]];
            mTmpBuf[i] = new GLfloat[100];
        }
        
        // Let's color the line
        _effect.useConstantColor = GL_TRUE;
        
        // Make the line a cyan color
        _effect.constantColor = GLKVector4Make(
                                                   0.0f, // Red
                                                   1.0f, // Green
                                                   0.0f, // Blue
                                                   1.0f);// Alpha
        
    }
    return self;
}

-(void)dealloc{
    for(int i = 0 ; i < 2; i ++){
        if(mBuf[i]) delete mBuf[i]; mBuf[i] = NULL;
        delete mTmpBuf[i]; mTmpBuf[i] = NULL;
    }
}

#pragma mark - GLKViewDelegate

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    glClearColor(0.f, 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // Prepare the effect for rendering
    [_effect prepareToDraw];
    
    for(int i = 0 ; i < 2 ; i++){
        // Create an handle for a buffer object array
        GLuint bufferObjectNameArray;
        
        // Have OpenGL generate a buffer name and store it in the buffer object array
        glGenBuffers(1, &bufferObjectNameArray);
        
        // Bind the buffer object array to the GL_ARRAY_BUFFER target buffer
        glBindBuffer(GL_ARRAY_BUFFER, bufferObjectNameArray);
        // Send the line data over to the target buffer in GPU RAM
        glBufferData(
                     GL_ARRAY_BUFFER,   // the target buffer
                     mIdx[i] * 2 * sizeof(GLfloat),       // the number of bytes to put into the buffer
                     mBuf[i],           // a pointer to the data being copied
                     GL_STATIC_DRAW);   // the usage pattern of the data
                
        // Enable vertex data to be fed down the graphics pipeline to be drawn
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        
        // Specify how the GPU looks up the data
        glVertexAttribPointer(
                              GLKVertexAttribPosition, // the currently bound buffer holds the data
                              2,                       // number of coordinates per vertex
                              GL_FLOAT,                // the data type of each component
                              GL_FALSE,                // can the data be scaled
                              2*4,                     // how many bytes per vertex (2 floats per vertex)
                              NULL);                   // offset to the first coordinate, in this case 0
        
        glLineWidth(2.0f);
        
        glDrawArrays(GL_LINE_STRIP, 0, mIdx[i]); // render
        
        glDeleteBuffers(1, &bufferObjectNameArray);
    }
}

#pragma mark - GLKViewControllerDelegate

- (void)glkViewControllerUpdate:(GLKViewController *)controller {
    BufferManager * bm = [_audioController getBufferManagerInstance];
    for(int i = 0 ; i < 2 ; i ++){
        mIdx[i] = bm->DrawBufferIdx(i);
        
        Float32 M=1,m=0;
        vDSP_maxv(bm->DrawBuffer(i), 1, &M, mIdx[i]);
        vDSP_minv(bm->DrawBuffer(i), 1, &m, mIdx[i]);
        for(UInt32 j = 0 ; j < mIdx[i] ; j ++){
            mBuf[i][2*j] = -1. + 2. * j / mLen[i];
            mBuf[i][2*j+1] = -1. + i + (bm->DrawBuffer(i)[j] - m) / (M - m) ;
        }
    }
}

@end

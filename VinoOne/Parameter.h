//
//  Parameter.h
//  VinoOne
//
//  Created by Ethan on 2014/10/23.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#ifndef VinoOne_Parameter_h
#define VinoOne_Parameter_h

struct Parameter{
    Float32     leftAmp;
    Float32     rightAmp;
    SInt32      window;
    SInt32      unit;
    SInt32      step;

    Parameter(): leftAmp(1.), rightAmp(1.), window(20), unit(5), step(0){printf("Construct\n");}
};

#endif

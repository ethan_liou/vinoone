//
//  AudioRenderer.h
//  VinoOne
//
//  Created by Ethan on 2014/10/23.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#ifndef __VinoOne__AudioRenderer__
#define __VinoOne__AudioRenderer__

#include <AudioUnit/AudioUnit.h>

OSStatus performRender (void                        *inRefCon,
                        AudioUnitRenderActionFlags 	*ioActionFlags,
                        const AudioTimeStamp 		*inTimeStamp,
                        UInt32 						inBusNumber,
                        UInt32 						inNumberFrames,
                        AudioBufferList             *ioData);

#endif /* defined(__VinoOne__AudioRenderer__) */

//
//  AppDelegate.h
//  VinoOne
//
//  Created by Ethan on 2014/10/22.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


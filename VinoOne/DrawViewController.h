//
//  DrawViewController.h
//  VinoOne
//
//  Created by Ethan on 2014/10/26.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "AudioController.h"

@interface DrawViewController : GLKViewController<GLKViewControllerDelegate>{
    UInt32 mIdx[2];
    UInt32 mLen[2];
    GLfloat* mBuf[2];
    GLfloat* mTmpBuf[2];
}

-(id)initWithAudioController:(AudioController*)controller;

@property (nonatomic, strong) GLKBaseEffect * effect;
@property (nonatomic, weak) AudioController * audioController;

@end

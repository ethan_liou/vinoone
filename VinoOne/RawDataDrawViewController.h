//
//  RawDataDrawViewController.h
//  VinoOne
//
//  Created by Ethan on 2014/11/7.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "AudioController.h"

@interface RawDataDrawViewController : GLKViewController<GLKViewControllerDelegate>{
    GLfloat* mBuf[2];
}

-(id)initWithAudioController:(AudioController*)controller;

@property (nonatomic, strong) GLKBaseEffect * effect;
@property (nonatomic, weak) AudioController * audioController;

@end

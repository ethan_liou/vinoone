//
//  MeasureViewController.m
//  VinoOne
//
//  Created by Ethan on 2014/10/22.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#import "MeasureViewController.h"
#import "FFTHelper.h"

@interface MeasureViewController ()

@end

@implementation MeasureViewController

void lowpass(DSPSplitComplex* freq, UInt32 len, Float32 freqInterval){
    for(UInt32 i = 0 ; i < len ; i ++){
        if(i > 100){
            freq->realp[i] = 0;
            freq->imagp[i] = 0;
        }
    }
}



- (IBAction)Measure:(UIButton*)sender{
    sender.enabled = NO;
    [_audioController moveToStep:2];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _audioController = [[AudioController alloc] init];
    [_audioController startIOUnit];
    _drawController = [[DrawViewController alloc] initWithAudioController:_audioController];
    _drawController.delegate = self;
    _drawController.preferredFramesPerSecond = 30;
    
    [self.view addSubview:_drawController.view];
    [self addChildViewController:_drawController];
    [_drawController didMoveToParentViewController:self];
    
    _drawController.view.frame = CGRectMake(self.view.bounds.size.width / 2 - 150, [[UIApplication sharedApplication] statusBarFrame].size.height + self.navigationController.navigationBar.frame.size.height + 10 , 300, 300);
    
    _rawController = [[RawDataDrawViewController alloc] initWithAudioController:_audioController];
    _rawController.delegate = _rawController;
    _rawController.preferredFramesPerSecond = 30;
    
    [self.view addSubview:_rawController.view];
    [self addChildViewController:_rawController];
    [_rawController didMoveToParentViewController:self];
    
    _rawController.view.frame = CGRectMake(_drawController.view.frame.origin.x, _drawController.view.frame.origin.y + 300+10, 300, 70);
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_audioController stopIOUnit];
    _audioController = nil;
}

#pragma mark - GLKViewControllerDelegate

- (void)glkViewControllerUpdate:(GLKViewController *)controller {
    [_drawController glkViewControllerUpdate:controller];
    _leftLbl.text = [NSString stringWithFormat:@"%.02f",[_audioController getLastValue:YES]];
    _rightLbl.text = [NSString stringWithFormat:@"%.02f",[_audioController getLastValue:NO]];
    _leftDiffLbl.text =[NSString stringWithFormat:@"%.02f ms",[_audioController getDiff:YES]];
    _rightDiffLbl.text =[NSString stringWithFormat:@"%.02f ms",[_audioController getDiff:NO]];
}

@end

//
//  AudioRenderer.cpp
//  VinoOne
//
//  Created by Ethan on 2014/10/23.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#include "AudioRenderer.h"
#include "CallbackData.h"
#include <sys/time.h>
#include <Accelerate/Accelerate.h>

UInt32 findPeak(Float32 * buffer, UInt32 stride, UInt32 len, Float32 threshold){ // buffer interleave
    Float32 maxVal;
    unsigned long maxIdx;
    vDSP_maxvi(buffer, stride, &maxVal, &maxIdx, len);
    return maxVal > threshold ? (UInt32)maxIdx/stride : len;
}

enum Step{ WaitFirstPeak, WaitSecondPeak, Sampling };
struct Context{
    Step mStep;
    UInt32 mCurrPeak;
    Float32 mBuffer[512];
    UInt32 mBufIdx;
    Context(Step step) : mStep(step){}
};

static Context context(WaitFirstPeak);
static bool left = true;

void InsertData(CallbackData* cd, Float32 data){
    cd->lastValue[left?0:1] = data;
    cd->bufferManager->InsertDrawData(data, left ? 0 : 1);
    if(!left && cd->bufferManager->IsPeak(1, cd->shiftLength)){
        cd->bufferManager->PrepareShift(cd->shiftLength);
        // trigger shift
        dispatch_semaphore_signal(cd->semaphore);
    }
    left = !left;
}

OSStatus performRender (void                        *inRefCon,
                        AudioUnitRenderActionFlags 	*ioActionFlags,
                        const AudioTimeStamp 		*inTimeStamp,
                        UInt32 						inBusNumber,
                        UInt32 						inNumberFrames,
                        AudioBufferList             *ioData){
    CallbackData * data = (CallbackData *)inRefCon;
    Parameter* param = &data->parameter;

    OSStatus err = AudioUnitRender(data->rioUnit, ioActionFlags, inTimeStamp, 1, inNumberFrames, ioData);
    
    // input
    Float32 *buffer = (Float32 *)ioData->mBuffers[0].mData;
    UInt32 interval = param->unit * param->window;
    if(context.mStep == WaitFirstPeak){
        context.mCurrPeak = findPeak(buffer, 2, inNumberFrames, 0.3);
        if(context.mCurrPeak != inNumberFrames){
            context.mStep = WaitSecondPeak;
        }
    } else if(context.mStep == WaitSecondPeak){
        UInt32 currPeak = findPeak(buffer, 2, inNumberFrames, 0.3);
        if(currPeak == inNumberFrames){
            // no peak, back to WaitFirstPeak
            context.mStep = WaitFirstPeak;
        } else {
            UInt32 diff = currPeak + inNumberFrames - context.mCurrPeak;
            if(diff % interval < param->unit/2 || diff % interval > interval - param->unit/2 ){
                context.mStep = Sampling;
                while (currPeak + interval < inNumberFrames) {
                    currPeak += interval;
                }
                if(currPeak + interval / 2 < inNumberFrames){
                    // there's right peak in this frames
                    currPeak += interval / 2;
                    left = false;
                }
                context.mCurrPeak = inNumberFrames - currPeak;
            }
        }
    } else{
        static int calIdx = -1;
        for(UInt32 i = 0 ; i < inNumberFrames ; i++){
            if(i == 0 && calIdx >= 0){
                InsertData(data, buffer[0]);
            }
            if(++context.mCurrPeak % (interval / 2) == 0 ){
                calIdx = i == inNumberFrames -1 ? 0 : -1;
                float M = buffer[2*i + 2];
                bool deferInsert = calIdx >= 0;
                if(i >= 25 && i < inNumberFrames - 25){
                    Float32 * b = data->bufferManager->Rawdata(left ? 0 : 1);
                    for(int j = 0 ; j < 50 ; j ++){
                        b[j] = buffer[2*(i - 25 + j)];
                    }
                }
                if(!deferInsert)
                    InsertData(data,M);
                context.mCurrPeak = 0;
            }
        }
    }
    // output
    assert(param->window / 2 != param->window / 2 + 1);
    static int outputOffset = 0;
    // Generate the samples
    int bufIdx = 0;
    for (UInt32 frame = 0; frame < inNumberFrames; frame++){
        if(outputOffset >= param->unit * param->window){
            outputOffset -= param->unit * param->window;
        }
        int div = outputOffset / param->unit;
        if(div == 0){
            buffer[bufIdx++] = -1.0f * param->leftAmp;
            buffer[bufIdx++] = 0.0f;
        } else if(div == 1){
            buffer[bufIdx++] = 1.0f * param->leftAmp;
            buffer[bufIdx++] = 0.0f;
        } else if(div == param->window / 2){
            buffer[bufIdx++] = 0.0f;
            buffer[bufIdx++] = context.mStep == Sampling ? -1.0f * param->rightAmp : 0.;
        } else if(div == param->window / 2 + 1){
            buffer[bufIdx++] = 0.0f;
            buffer[bufIdx++] = context.mStep == Sampling ? 1.0f * param->rightAmp : 0.;
        } else{
            buffer[bufIdx++] = 0.0f;
            buffer[bufIdx++] = 0.0f;
        }
        outputOffset++;
    }
    
    return err;
}
//
//  CallbackData.h
//  VinoOne
//
//  Created by Ethan on 2014/10/23.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#ifndef VinoOne_CallbackData_h
#define VinoOne_CallbackData_h

#include "BufferManager.h"
#include "Parameter.h"
#include <AudioUnit/AudioUnit.h>

struct CallbackData{
    AudioUnit               rioUnit;
    BufferManager*          bufferManager;
    bool*                   audioChainIsBeingReconstructed;
    struct Parameter        parameter;
    float                   lastValue[2];
    float                   diff[2];
    dispatch_semaphore_t    semaphore;
    UInt32                  shiftLength;
    
    CallbackData(): rioUnit(NULL), bufferManager(NULL), audioChainIsBeingReconstructed(NULL), shiftLength(240){
        semaphore = dispatch_semaphore_create(0);
    }
};


#endif

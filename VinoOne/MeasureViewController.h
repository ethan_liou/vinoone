//
//  MeasureViewController.h
//  VinoOne
//
//  Created by Ethan on 2014/10/22.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "AudioController.h"
#import "DrawViewController.h"
#import "RawDataDrawViewController.h"

@interface MeasureViewController : UIViewController<GLKViewControllerDelegate>{
    AudioController * _audioController;
    DrawViewController * _drawController;
    RawDataDrawViewController * _rawController;
    IBOutlet UILabel * _leftLbl;
    IBOutlet UILabel * _rightLbl;
    IBOutlet UILabel * _leftDiffLbl;
    IBOutlet UILabel * _rightDiffLbl;
}

@end

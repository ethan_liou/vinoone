//
//  AudioController.h
//  VinoOne
//
//  Created by Ethan on 2014/10/22.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "BufferManager.h"
#import "CallbackData.h"

@interface AudioController : NSObject{
    AudioUnit               _rioUnit;
    BufferManager*          _bufferManager;
    bool                    _audioChainIsBeingReconstructed;
    struct CallbackData     _cd;
    dispatch_queue_t        _shiftQueue;
}

- (OSStatus)    startIOUnit;
- (OSStatus)    stopIOUnit;
- (BufferManager*) getBufferManagerInstance;
- (void) updateAmp:(BOOL)left WithAmp:(Float32)ampValue;
- (Float32) getAmp:(BOOL)left;
- (void) moveToStep:(SInt32)step;
- (SInt32) getStep;
- (Float32) getDiff:(BOOL)left;
- (Float32) getLastValue:(BOOL)left;

@end

//
//  AudioController.m
//  VinoOne
//
//  Created by Ethan on 2014/10/22.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#import "AudioController.h"

// Framework includes
#import <AVFoundation/AVAudioSession.h>

// Utility file includes
#import "CAXException.h"
#import "CAStreamBasicDescription.h"

#import "AudioRenderer.h"
#import <vector>
#import <Accelerate/Accelerate.h>

#define SAMPLE_RATE 48000
#define DRAW_LEN 4096

@interface AudioController()

- (void)setupAudioSession;
- (void)setupIOUnit;
- (void)setupAudioChain;

@end

@implementation AudioController

- (id)init
{
    if (self = [super init]) {
        _bufferManager = NULL;
        _shiftQueue = dispatch_queue_create( "shiftqueue", NULL );
        [self setupAudioChain];
    }
    return self;
}

- (void)dealloc{
    dispatch_suspend(_shiftQueue);
    NSLog(@"Dealloc");
}

- (OSStatus)startIOUnit
{
    OSStatus err = AudioOutputUnitStart(_rioUnit);
    if (err) NSLog(@"couldn't start AURemoteIO: %d", (int)err);
    return err;
}

- (OSStatus)stopIOUnit
{
    OSStatus err = AudioOutputUnitStop(_rioUnit);
    if (err) NSLog(@"couldn't stop AURemoteIO: %d", (int)err);
    return err;
}

- (BufferManager*) getBufferManagerInstance{
    return _bufferManager;
}

- (void)handleInterruption:(NSNotification *)notification
{
    try {
        UInt8 theInterruptionType = [[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] intValue];
        NSLog(@"Session interrupted > --- %s ---\n", theInterruptionType == AVAudioSessionInterruptionTypeBegan ? "Begin Interruption" : "End Interruption");
        
        if (theInterruptionType == AVAudioSessionInterruptionTypeBegan) {
            [self stopIOUnit];
        }
        
        if (theInterruptionType == AVAudioSessionInterruptionTypeEnded) {
            // make sure to activate the session
            NSError *error = nil;
            [[AVAudioSession sharedInstance] setActive:YES error:&error];
            if (nil != error) NSLog(@"AVAudioSession set active failed with error: %@", error);
            
            [self startIOUnit];
        }
    } catch (CAXException e) {
        char buf[256];
        fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf, 256));
    }
}

- (void)handleRouteChange:(NSNotification *)notification
{
    UInt8 reasonValue = [[notification.userInfo valueForKey:AVAudioSessionRouteChangeReasonKey] intValue];
    AVAudioSessionRouteDescription *routeDescription = [notification.userInfo valueForKey:AVAudioSessionRouteChangePreviousRouteKey];
    
    NSLog(@"Route change:");
    switch (reasonValue) {
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            NSLog(@"     NewDeviceAvailable");
            break;
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            NSLog(@"     OldDeviceUnavailable");
            break;
        case AVAudioSessionRouteChangeReasonCategoryChange:
            NSLog(@"     CategoryChange");
            NSLog(@" New Category: %@", [[AVAudioSession sharedInstance] category]);
            break;
        case AVAudioSessionRouteChangeReasonOverride:
            NSLog(@"     Override");
            break;
        case AVAudioSessionRouteChangeReasonWakeFromSleep:
            NSLog(@"     WakeFromSleep");
            break;
        case AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory:
            NSLog(@"     NoSuitableRouteForCategory");
            break;
        default:
            NSLog(@"     ReasonUnknown");
    }
    
    NSLog(@"Previous route:\n");
    NSLog(@"%@", routeDescription);
}

- (void)handleMediaServerReset:(NSNotification *)notification
{
    NSLog(@"Media server has reset");
    _audioChainIsBeingReconstructed = YES;
    
    usleep(25000); //wait here for some time to ensure that we don't delete these objects while they are being accessed elsewhere
    
    // rebuild the audio chain
    delete _bufferManager;  _bufferManager = NULL;
    
    [self setupAudioChain];
    [self startIOUnit];
    
    _audioChainIsBeingReconstructed = NO;
}

- (void)setupAudioSession
{
    try {
        // Configure the audio session
        AVAudioSession *sessionInstance = [AVAudioSession sharedInstance];
        
        // we are going to play and record so we pick that category
        NSError *error = nil;
        [sessionInstance setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
        XThrowIfError((OSStatus)error.code, "couldn't set session's audio category");
        
        // set the buffer duration to 5 ms
        NSTimeInterval bufferDuration = .005;
        [sessionInstance setPreferredIOBufferDuration:bufferDuration error:&error];
        XThrowIfError((OSStatus)error.code, "couldn't set session's I/O buffer duration");
        
        // set the session's sample rate
        [sessionInstance setPreferredSampleRate:SAMPLE_RATE error:&error];
        XThrowIfError((OSStatus)error.code, "couldn't set session's preferred sample rate");
        
        // add interruption handler
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleInterruption:)
                                                     name:AVAudioSessionInterruptionNotification
                                                   object:sessionInstance];
        
        // we don't do anything special in the route change notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleRouteChange:)
                                                     name:AVAudioSessionRouteChangeNotification
                                                   object:sessionInstance];
        
        // if media services are reset, we need to rebuild our audio chain
        [[NSNotificationCenter defaultCenter]	addObserver:	self
                                                 selector:	@selector(handleMediaServerReset:)
                                                     name:	AVAudioSessionMediaServicesWereResetNotification
                                                   object:	sessionInstance];
        
        // activate the audio session
        [[AVAudioSession sharedInstance] setActive:YES error:&error];
        XThrowIfError((OSStatus)error.code, "couldn't set session active");
    }
    
    catch (CAXException &e) {
        NSLog(@"Error returned from setupAudioSession: %d: %s", (int)e.mError, e.mOperation);
    }
    catch (...) {
        NSLog(@"Unknown error returned from setupAudioSession");
    }
    
    return;
}

- (void) logParam {
    Float64 sampleRate;
    UInt32 srSize = sizeof (sampleRate);
    OSStatus error =
    AudioSessionGetProperty(
                            kAudioSessionProperty_CurrentHardwareSampleRate,
                            &srSize,
                            &sampleRate);
    if (error == noErr) {
        NSLog (@"CurrentHardwareSampleRate = %f", sampleRate);
    }
    UInt32 inputGainAvailable = 0;
    UInt32 inputGainAvailableSize = sizeof(inputGainAvailable);
    error = AudioSessionGetProperty(
                                    kAudioSessionProperty_InputGainAvailable,
                                    &inputGainAvailableSize,
                                    &inputGainAvailable);
    if (error == noErr) {
        NSLog (@"gain = %u", inputGainAvailable);
    }
    Float32 inputGain=1.0f;
    error = AudioSessionSetProperty(
                                    kAudioSessionProperty_InputGainScalar,
                                    sizeof(inputGain),
                                    &inputGain);
    if (error == noErr) {
        NSLog (@"gain OK");
    }
    
}

- (void)setupIOUnit
{
    try {
        // Create a new instance of AURemoteIO
        
        AudioComponentDescription desc;
        desc.componentType = kAudioUnitType_Output;
        desc.componentSubType = kAudioUnitSubType_RemoteIO;
        desc.componentManufacturer = kAudioUnitManufacturer_Apple;
        desc.componentFlags = 0;
        desc.componentFlagsMask = 0;
        
        AudioComponent comp = AudioComponentFindNext(NULL, &desc);
        XThrowIfError(AudioComponentInstanceNew(comp, &_rioUnit), "couldn't create a new instance of AURemoteIO");
        
        UInt32 allowBluetoothInput = 1;

        AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryEnableBluetoothInput,
            sizeof (allowBluetoothInput),
            &allowBluetoothInput);

        
        //  Enable input and output on AURemoteIO
        //  Input is enabled on the input scope of the input element
        //  Output is enabled on the output scope of the output element
        
        UInt32 one = 1;
        XThrowIfError(AudioUnitSetProperty(_rioUnit, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Input, 1, &one, sizeof(one)), "could not enable input on AURemoteIO");
        XThrowIfError(AudioUnitSetProperty(_rioUnit, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Output, 0, &one, sizeof(one)), "could not enable output on AURemoteIO");
        
        // Explicitly set the input and output client formats
        
        CAStreamBasicDescription ioFormat = CAStreamBasicDescription(SAMPLE_RATE, 2, CAStreamBasicDescription::kPCMFormatFloat32, true);
        XThrowIfError(AudioUnitSetProperty(_rioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 1, &ioFormat, sizeof(ioFormat)), "couldn't set the input client format on AURemoteIO");
        XThrowIfError(AudioUnitSetProperty(_rioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &ioFormat, sizeof(ioFormat)), "couldn't set the output client format on AURemoteIO");
        
        // Set the MaximumFramesPerSlice property. This property is used to describe to an audio unit the maximum number
        // of samples it will be asked to produce on any single given call to AudioUnitRender
        UInt32 maxFramesPerSlice = 4096;
        XThrowIfError(AudioUnitSetProperty(_rioUnit, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 0, &maxFramesPerSlice, sizeof(UInt32)), "couldn't set max frames per slice on AURemoteIO");
        
        // Get the property value back from AURemoteIO. We are going to use this value to allocate buffers accordingly
        UInt32 propSize = sizeof(UInt32);
        XThrowIfError(AudioUnitGetProperty(_rioUnit, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 0, &maxFramesPerSlice, &propSize), "couldn't get max frames per slice on AURemoteIO");
        
        _bufferManager = new BufferManager(DRAW_LEN);
        
        _cd.rioUnit = _rioUnit;
        _cd.bufferManager = _bufferManager;
        _cd.audioChainIsBeingReconstructed = &_audioChainIsBeingReconstructed;
        
        // Set the render callback on AURemoteIO
        AURenderCallbackStruct renderCallback;
        renderCallback.inputProc = performRender;
        renderCallback.inputProcRefCon = &_cd;
        XThrowIfError(AudioUnitSetProperty(_rioUnit, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input, 0, &renderCallback, sizeof(renderCallback)), "couldn't set render callback on AURemoteIO");
        
        // Initialize the AURemoteIO instance
        XThrowIfError(AudioUnitInitialize(_rioUnit), "couldn't initialize AURemoteIO instance");
        [self logParam];
    }
    
    catch (CAXException &e) {
        NSLog(@"Error returned from setupIOUnit: %d: %s", (int)e.mError, e.mOperation);
    }
    catch (...) {
        NSLog(@"Unknown error returned from setupIOUnit");
    }
    
    return;
}

Float32 subMeanVec[50];
Float32 squared[50];

- (Float32)stdevWithPtr:(Float32*)ptr Length:(UInt32)length{
    float mean = 0;
    vDSP_meanv(ptr, 1, &mean, length); // calc mean
    mean = -1*mean;
    vDSP_vsadd(ptr,1,&mean,subMeanVec,1,length); // x - mean
    vDSP_vsq(subMeanVec,1,squared,1,length); // (x - mean)^2
    Float32 sum = 0;
    vDSP_sve(squared,1,&sum,length); // sum ( (x-mean) ^ 2 )
    return sqrt(sum/length-1); // sqrt ( sum / length )
}

- (void)verifyFinish:(Float32)data{
    static std::vector<Float32> datas(30);
    if(datas.size() == 30){
        datas.erase(datas.begin());
    }
    datas.push_back(data);
    _cd.diff[1] = [self stdevWithPtr:datas.data() Length:datas.size()];
}

- (void)setupAudioChain{
    [self setupAudioSession];
    [self setupIOUnit];
    
    // run shift queue
    dispatch_async(_shiftQueue, ^{
        int cnt = 0;
        Float32 total = 0;
        while ( YES ) {
            dispatch_semaphore_wait(_cd.semaphore, DISPATCH_TIME_FOREVER);
            Float32 data = 1. * _bufferManager->DoShift(_cd.shiftLength) * _cd.parameter.window * _cd.parameter.unit / 48;
            [self verifyFinish:data];
            total += data;
            cnt ++;
            if(cnt == 5){
                _cd.diff[0] = total / cnt;
                total = 0;
                cnt = 0;
            }
        }
    });
}

- (void)updateAmp:(BOOL)left WithAmp:(Float32)ampValue{
    Float32 * amp = left ? &_cd.parameter.leftAmp : &_cd.parameter.rightAmp;
    *amp = ampValue;
}

- (Float32)getAmp:(BOOL)left{
    return left ? _cd.parameter.leftAmp : _cd.parameter.rightAmp;
}

- (void) moveToStep:(SInt32)step{
    _cd.parameter.step = step;
}

- (SInt32) getStep{
    return _cd.parameter.step;
}

- (Float32) getDiff:(BOOL)left{
    return _cd.diff[left?0:1];
}

- (Float32) getLastValue:(BOOL)left{
    return _cd.lastValue[left?0:1];
}

@end

//
//  BufferManager.h
//  VinoOne
//
//  Created by Ethan on 2014/10/22.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#ifndef __VinoOne__BufferManager__
#define __VinoOne__BufferManager__

#include <AudioToolbox/AudioToolbox.h>
#include <libkern/OSAtomic.h>

class Buffer{
public:
    Buffer(UInt32 frames);
    ~Buffer();
    UInt32          mFrames;
    Float32*        mDrawBuffer;
    UInt32          mDrawBufferIdx;
    UInt32          mDrawBufferLen;
};

class BufferManager{
public:
    BufferManager( UInt32 inMaxFramesPerSlice );
    ~BufferManager();
    UInt32          InsertDrawData(Float32 data,UInt32 idx);
    bool            InsertFFTData(Float32 data,UInt32 idx);
    UInt32          DrawBufferIdx(UInt32 idx) const { return mBuffer[idx]->mDrawBufferIdx; }
    UInt32          DrawBufferLen(UInt32 idx) const { return mBuffer[idx]->mDrawBufferLen; }
    Float32*        DrawBuffer(UInt32 idx) const { return mBuffer[idx]->mDrawBuffer; }
    bool            IsPeak(UInt32 bufIdx, UInt32 len);
    bool            IsValley(UInt32 bufIdx, UInt32 idx, UInt32 len);
    UInt32          FindMidPt(UInt32 bufIdx, UInt32 startIdx, UInt32 len);
    void            PrepareShift(UInt32 len);
    unsigned long   DoShift(UInt32 len);
    Float32*        Rawdata(UInt32 idx);
private:
    void            InsertToBuffer(Float32 data, Float32 * buffer, UInt32& idx, UInt32 len);
    Buffer*         mBuffer[2];
    Float32*        mShiftBuffer[2];
    Float32*        mResBuffer;
    Float32*        mRawdata[2];
};


#endif /* defined(__VinoOne__BufferManager__) */

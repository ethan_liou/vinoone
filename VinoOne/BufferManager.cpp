//
//  BufferManager.cpp
//  VinoOne
//
//  Created by Ethan on 2014/10/22.
//  Copyright (c) 2014年 Vinotechs. All rights reserved.
//

#include "BufferManager.h"
#include <Accelerate/Accelerate.h>

Buffer::Buffer(UInt32 frames){
    mFrames = frames;
    mDrawBufferIdx = 0;
    mDrawBufferLen = frames;
    mDrawBuffer = new Float32[mDrawBufferLen];
    memset(mDrawBuffer,0, mDrawBufferLen * sizeof(Float32));
}

Buffer::~Buffer(){
    if(mDrawBuffer) delete mDrawBuffer; mDrawBuffer = NULL;
}

BufferManager::BufferManager( UInt32 inMaxFramesPerSlice ){
    mBuffer[0] = new Buffer(inMaxFramesPerSlice);
    mBuffer[1] = new Buffer(inMaxFramesPerSlice);
    mShiftBuffer[0] = new Float32[480];
    mShiftBuffer[1] = new Float32[480];
    mResBuffer = new Float32[100];
    mRawdata[0] = new Float32[50];
    mRawdata[1] = new Float32[50];
}

BufferManager::~BufferManager(){
    if(mBuffer[0]) delete mBuffer[0]; mBuffer[0] = NULL;
    if(mBuffer[1]) delete mBuffer[1]; mBuffer[1] = NULL;
    if(mShiftBuffer[0]) delete mShiftBuffer[0]; mShiftBuffer[0] = NULL;
    if(mShiftBuffer[1]) delete mShiftBuffer[1]; mShiftBuffer[1] = NULL;
    if(mResBuffer) delete mResBuffer; mResBuffer = NULL;
    if(mRawdata[0]) delete mRawdata[0]; mRawdata[0] = NULL;
    if(mRawdata[1]) delete mRawdata[1]; mRawdata[1] = NULL;
}

void BufferManager::InsertToBuffer(Float32 data, Float32 * buffer, UInt32& idx, UInt32 len){
    if (idx != len) {
        idx++;
    }
//    memmove(buffer, buffer+1, (idx-1) * sizeof(Float32));
//    buffer[idx-1] = data;
    memmove(buffer+1, buffer, (idx-1)*sizeof(Float32));
    buffer[0] = data;
}

UInt32 BufferManager::InsertDrawData(Float32 data, UInt32 idx){
    InsertToBuffer(data, mBuffer[idx]->mDrawBuffer, mBuffer[idx]->mDrawBufferIdx, mBuffer[idx]->mDrawBufferLen);
    return mBuffer[idx]->mDrawBufferIdx;
}

bool BufferManager::IsPeak(UInt32 bufIdx, UInt32 len){
    if(mBuffer[bufIdx]->mDrawBufferIdx < len)
        return false;
    Float32 maxVal;
    unsigned long maxIdx;
    vDSP_maxvi(&mBuffer[bufIdx]->mDrawBuffer[mBuffer[bufIdx]->mDrawBufferIdx-len], 1, &maxVal, &maxIdx, len);
        
    return maxIdx == len / 2;
}

static int counter = 0;

UInt32 BufferManager::FindMidPt(UInt32 bufIdx, UInt32 startIdx, UInt32 len){
    Float32 * buf = mBuffer[bufIdx]->mDrawBuffer;
    Float32 maxVal = 0;
    unsigned long maxIdx = 0;
    Float32 minVal = buf[startIdx];
    vDSP_maxvi(&buf[startIdx], 1, &maxVal, &maxIdx, len);
    if(bufIdx == 0){
        for(UInt32 i = startIdx + 1 ; i < startIdx + len ; i++){
            printf("c%d %f\n",counter, buf[i]);
        }
        counter++;
    }
    if(maxIdx == len - 1 || maxIdx < 20){
        return 0;
    } else {
        Float32 midVal = ( buf[startIdx] + maxVal ) / 2;
        printf("%u %ld %f %f %f %f\n",bufIdx, maxIdx, maxVal, midVal, minVal, 100. * (maxVal - minVal) / minVal);

        for(UInt32 i = startIdx + 1 ; i < startIdx + maxIdx ; i++){
            if(buf[i] > midVal)
                return i - startIdx;
        }
    }
    return 0;
}

bool BufferManager::IsValley(UInt32 bufIdx, UInt32 idx, UInt32 len){
    assert(idx >= len/2);
    bool valley = true;
    for(UInt32 i = idx - len / 2 ; i < idx ; i++){
        if(mBuffer[bufIdx]->mDrawBuffer[i] < mBuffer[bufIdx]->mDrawBuffer[idx]){
            valley = false;
            break;
        }
    }
    if(valley){
        for (UInt32 i = idx + 1; i < idx + len / 2; i++) {
            if(mBuffer[bufIdx]->mDrawBuffer[i] <= mBuffer[bufIdx]->mDrawBuffer[idx]){
                valley = false;
                break;
            }
        }
    }
    return valley;
}

void BufferManager::PrepareShift(UInt32 len){
    memcpy(mShiftBuffer[0], &mBuffer[0]->mDrawBuffer[mBuffer[0]->mDrawBufferIdx - len], len * sizeof(Float32));
    memcpy(mShiftBuffer[1], &mBuffer[1]->mDrawBuffer[mBuffer[1]->mDrawBufferIdx - len], len * sizeof(Float32));
}

unsigned long BufferManager::DoShift(UInt32 len){
    // normalize
    for(int i = 0 ; i < 2 ; i ++){
        Float32 m,M;
        vDSP_maxv(mShiftBuffer[i], 1, &M, len);
        vDSP_minv(mShiftBuffer[i], 1, &m, len);
        Float32 B = 1. / (M - m);
        Float32 C = -m / (M - m);
        vDSP_vsmsa(mShiftBuffer[i], 1, &B, &C, mShiftBuffer[i], 1, len);
    }
    
    int inc = 0;
    int resLen = 100;
    for(int i = 0 ; i < 100 ; i++){
        vDSP_distancesq(mShiftBuffer[0], 1, &mShiftBuffer[1][i], 1, &mResBuffer[i], len - i);
        if(i > 0 && mResBuffer[i] > mResBuffer[i-1]){
            inc++;
        } else {
            inc = 0;
        }
        if(inc >= 5){
            resLen = i + 1;
            break;
        }
    }
    
    Float32 minVal;
    unsigned long minIdx;
    vDSP_minvi(mResBuffer, 1, &minVal, &minIdx, resLen);
    return minIdx;
}

Float32* BufferManager::Rawdata(UInt32 idx){
    return mRawdata[idx];
}

